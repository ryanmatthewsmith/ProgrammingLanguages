(* Dan Grossman, CSE341, HW3 Provided Code *)

exception NoAnswer

datatype pattern = WildcardP
		 | VariableP of string
		 | UnitP
		 | ConstantP of int
		 | ConstructorP of string * pattern
		 | TupleP of pattern list

datatype valu = Constant of int
	      | Unit
	      | Constructor of string * valu
	      | Tuple of valu list

fun g f1 f2 p =
    let 
	val r = g f1 f2 
    in
	case p of
	    WildcardP         => f1 ()
	  | VariableP x       => f2 x
	  | ConstructorP(_,p) => r p
	  | TupleP ps         => List.foldl (fn (p,i) => (r p) + i) 0 ps
	  | _                 => 0
    end

(**** for the challenge problem only ****)

datatype typ = AnythingT
	     | UnitT
	     | IntT
	     | TupleT of typ list
	     | DatatypeT of string

(* ryan matthew smith, rsmith24, HW3 solutions *)

(* 1 *)
fun only_lowercase strList =
  List.filter (fn s => Char.isLower(String.sub(s,0))) strList

(* 2 *)
fun longest_string1 strList =
  List.foldl (fn (st1,st2) => if String.size st1 > String.size st2 then st1 else st2) "" strList 

(* 3 *)
fun longest_string2 strList =
  List.foldl (fn (st1,st2) => if String.size st1 >= String.size st2 then st1 else st2) "" strList

(* 4 *)
fun longest_string_helper cmp strList =
  List.foldl (fn (st1,st2) => if cmp (String.size st1 , String.size st2) then st1 else st2) "" strList

val longest_string3 = longest_string_helper (fn (c1,c2) => c1 > c2) 

val longest_string4 = longest_string_helper (fn (c1,c2) => c1 >= c2)

(* 5 *)
fun longest_lowercase strList =
  let 
    val combo = longest_string3 o only_lowercase 
  in
    combo strList
  end

(* 6 *)
fun rev_string str =
  let
    val upperString = String.map Char.toUpper str
  in
    (String.implode o List.rev o String.explode) upperString
  end

(* 7 *)
fun first_answer f lst =
  case lst of
    [] => raise NoAnswer
    | front::lst' => let 
                  val v = f front
                in
                  if isSome v
                  then valOf v
                  else first_answer f lst'
                end

(* 8 *)
fun all_answers f lst =
  let 
    fun helper_append (acc, lst) =
      case lst of
        [] => SOME acc
        | front::lst' => case f front of
                         SOME v => helper_append(acc @ v,lst')
                         | NONE => NONE
  in
    helper_append ([], lst)
  end

(* 9 *)
(* a *)

(* g is a curried function that has two function arguments and a pattern datatype argument.
   the first function argument f1 is (unit->int), f2 is (string -> int). The final output of
   the function is an int.  g applies the functions to the correct patterns with a case
   statement that applies f1, f2, or both to the pattern, a tuple is folded left to repeat. *)

(* b *)
fun count_wildcards p = g (fn x => 1) (fn x => 0) p

(* c *)
fun count_wild_and_variable_lengths p = g (fn x => 1) (fn x => String.size x) p

(* d *)
fun count_a_var (str, p) =
  g (fn x => 0) (fn x => if x = str then 1 else 0) p

(* 10 *)
fun check_pat p =
  let 
    fun varsToStrings p =
      case p of
        VariableP s => [s]
        | TupleP pt => List.foldr (fn (p, x) => x @ varsToStrings p) [] pt
        | ConstructorP(_,p) => varsToStrings p
        | _ => []
    fun areNoDupes lst =
      case lst of
        [] => true
        | front::lst' => if List.exists (fn next => next = front) lst' then false else areNoDupes lst'
  in
    (areNoDupes o varsToStrings) p
  end

(* 11 *)
fun match (v, p) =
  case p of
    WildcardP => SOME []
    | VariableP s => SOME [(s,v)]
    | UnitP => ( case v of 
                   Unit => SOME []
                   | _ => NONE )
    | ConstantP x => ( case v of
                         Constant y => if x = y
                                       then SOME []
                                       else NONE
                         | _ => NONE )
    | ConstructorP(s1,p) => ( case v of
                               Constructor(s2,v1) => if s1 = s2
                                                     then match(v1,p)
                                                     else NONE 
                               | _ => NONE )
    | TupleP ps => ( case v of
                       Tuple(vs) =>  if (List.length ps = List.length vs)
                                     then  all_answers match (ListPair.zip(vs,ps)) 
                                     else NONE
                       | _ => NONE )
    
(* 12 *)
fun first_match (v, ps) = 
    SOME (first_answer (fn p => match(v,p)) ps) 
    handle NoAnswer => NONE 
   








