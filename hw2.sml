(* CSE 341, Spring 2017, HW2 Provided Code *)
(* Ryan Matthew Smith rsmith24 *)

(* main datatype definition we will use throughout the assignment *)
datatype json =
         Num of real (* real is what SML calls floating point numbers *)
       | String of string
       | False
       | True
       | Null
       | Array of json list
       | Object of (string * json) list

(* some examples of values of type json *)
val json_pi    = Num 3.14159
val json_hello = String "hello"
val json_false = False
val json_array = Array [Num 1.0, String "world", Null]
val json_obj   = Object [("foo", json_pi), ("bar", json_array), ("ok", True)]

(* some provided one-liners that use the standard library and/or some features
   we have not learned yet. (Only) the challenge problem will need more
   standard-library functions. *)

(* dedup : string list -> string list -- it removes duplicates *)
fun dedup xs = ListMergeSort.uniqueSort String.compare xs

(* strcmp : string * string -> order compares strings alphabetically
   where datatype order = LESS | EQUAL | GREATER *)
val strcmp = String.compare                                        
                        
(* convert an int to a real *)
val int_to_real = Real.fromInt

(* absolute value of a real *)
val real_abs = Real.abs

(* convert a real to a string *)
val real_to_string = Real.toString
                   
(* We now load 3 files with police data represented as values of type json.
   Each file binds one variable: small_incident_reports (10 reports), 
   medium_incident_reports (100 reports), and large_incident_reports 
   (1000 reports) respectively.

   However, the large file is commented out for now because it will take 
   about 15 seconds to load, which is too long while you are debugging
   earlier problems.  In string format, we have ~10000 records -- if you
   do the challenge problem, you will be able to read in all 10000 quickly --
   it's the "trick" of giving you large SML values that is slow.
*)

(* Make SML print a little less while we load a bunch of data. *)
       ; (* this semicolon is important -- it ends the previous binding *)
Control.Print.printDepth := 3;
Control.Print.printLength := 3;

use "parsed_small_police.sml";
use "parsed_medium_police.sml";
use "parsed_large_police.sml"; 

val large_incident_reports_list =
    case large_incident_reports of
        Array js => js
      | _ => raise (Fail "expected large_incident_reports to be an array")


(* Now make SML print more again so that we can see what we're working with. *)
; Control.Print.printDepth := 20;
Control.Print.printLength := 20;

(**** PUT PROBLEMS 1-8 HERE ****)
(* 1 silly json format *)
fun make_silly_json i =
  let fun silly_object_list i =
    if i < 1
    then []
    else
      Object[("n",Num (int_to_real i)),("b",True)]::silly_object_list(i-1)
  in
    Array (silly_object_list(i))
  end

(* 2 returns first v1 where k matches k1 *)
fun assoc (k, xs) =
  case xs of
      [] => NONE
    | (k1,v1)::xs' => if k1 = k then SOME v1 else assoc(k,xs')

(* 3 returns a value based on the passed json j, matching field f  *)
fun dot (j, f) = 
  case j of
    Object(a) => assoc(f,a)
    | _ => NONE

(* 4 returns list of field names if the passed json is a non empty json object *)
fun one_fields json =
  let fun tail_one_fields (json, acc) =
    case json of
      Object(a) => ( case a of
                       [] => acc
                       | (names,v)::json' => tail_one_fields(Object json',names::acc) )
      | _ => acc
  in
    tail_one_fields(json, [])
  end

(* 5 true if no str is repeated in provided list *)
fun no_repeats strList =
  length(strList) = length(dedup(strList))

(* 6  Checks for duplicate fields of objects in the json list, true for no dupes *)
fun recursive_no_field_repeats json =
  let 
    fun process_array json =
      case json of
        Array(a) => ( case a of
                        [] => true
                        | front::json' => recursive_no_field_repeats(front)
                                          andalso process_array(Array json') )
        | _ => true

    fun process_object json =
      if not (no_repeats(one_fields(json)))
      then false
      else
        case json of
          Object(a) => (case a of
                         [] => true
                         | (f,jsonIn)::json' => case jsonIn of
                                                  Object(a) => process_object(jsonIn) andalso process_object(Object json')
                                                  | Array(b) => process_array(jsonIn) andalso process_object(Object json')
                                                  | _ => recursive_no_field_repeats (Object json') )
         | _ => true
  in
    case json of
      Array (a) => process_array(json)
      | Object(a) => process_object(json)
      | _ => true
  end

(* 7 returns str * int list where the int is occurances, str is occurances in the provided list  *)
fun count_occurrences (strList : string list, ex : exn) =
  let 
    fun tailCount (strList, count, outList) =
      case strList of
        [] => outList
        | last::[] => (last, count)::outList
        | curr::next::strList' => if strcmp(curr,next) = GREATER
                                  then raise ex
                                  else if strcmp(curr,next) = EQUAL
                                       then tailCount(next::strList',count+1,outList)
                                       else tailCount(next::strList',1,(curr,count)::outList)
  in
    tailCount(strList, 1, [])
  end

(* 8 returns list of equal to field json, provided json map is also a String *)
fun string_values_for_field (str, jsonList : (json list)) =
  case jsonList of
    [] => []
    | json::jsonList' => case dot(json,str) of
                         SOME (String(a)) => a::string_values_for_field(str, jsonList')
                         | _ => string_values_for_field(str,jsonList')

(* histogram and historgram_for_field are provided, but they use your 
   count_occurrences and string_values_for_field, so uncomment them 
   after doing earlier problems *)

(* histogram_for_field takes a field name f and a list of objects js and 
   returns counts for how often a string is the contents of f in js. *)

exception SortIsBroken

fun histogram (xs : string list) : (string * int) list =
  let
    fun compare_strings (s1 : string, s2 : string) : bool = s1 > s2

    val sorted_xs = ListMergeSort.sort compare_strings xs
    val counts = count_occurrences (sorted_xs,SortIsBroken)

    fun compare_counts ((s1 : string, n1 : int), (s2 : string, n2 : int)) : bool =
      n1 < n2 orelse (n1 = n2 andalso s1 < s2)
  in
    ListMergeSort.sort compare_counts counts
  end

fun histogram_for_field (f,js) =
  histogram (string_values_for_field (f, js))


(**** PUT PROBLEMS 9-11 HERE ****)
(* 9 returns subset of json list, with objects that match field and string  *)
fun filter_field_value (key, str, json) =
  case json of
    [] => []
    | next::json' => case dot(next,key) of
                       SOME (String(a)) => if strcmp(a,str) = EQUAL
                                         then (next::filter_field_value(key, str, json'))
                                         else filter_field_value(key, str, json')
                       | _ => filter_field_value(key, str, json')


(* 10 Event clearance histogram *)
val large_event_clearance_description_histogram = 
      histogram_for_field("event_clearance_description",
                           large_incident_reports_list)

(* 11 hundred block histogram *)
val large_hundred_block_location_histogram = 
      histogram_for_field("hundred_block_location",
                           large_incident_reports_list)

;Control.Print.printDepth := 3;
Control.Print.printLength := 3;

(**** PUT PROBLEMS 12-15 HERE ****)
(* 12 43XX ave reports json list *)
val forty_third_and_the_ave_reports = filter_field_value("hundred_block_location",
                                                         "43XX BLOCK OF UNIVERSITY WAY NE",
                                                         large_incident_reports_list);

(* 13 43XX ave clearance reports *)
val forty_third_and_the_ave_event_clearance_description_histogram = 
      histogram_for_field("event_clearance_description",
        forty_third_and_the_ave_reports)      

(* 14 45XX on 19th reports *)
val nineteenth_and_forty_fifth_reports = filter_field_value("hundred_block_location",
                                                            "45XX BLOCK OF 19TH AVE NE",
                                                            large_incident_reports_list);

(* 15 45XX on 19th clearance *)
val nineteenth_and_forty_fifth_event_clearance_description_histogram = 
      histogram_for_field("event_clearance_description",
        nineteenth_and_forty_fifth_reports)

;Control.Print.printDepth := 20;
Control.Print.printLength := 20;

(**** PUT PROBLEMS 16-19 HERE ****)

(* 16 concatenates a string list into a string with the provided divider in between original strings *)
fun concat_with (divi : string, strList : string list) =
  case strList of
    [] => ""
    | str::[] => str
    | str::strList' => str ^ divi ^ concat_with(divi, strList')

(* 17 adds extra " to the passed string *)
fun quote_string str =
  "" ^ "\"" ^ str ^ "\""

(* 18 String representation of provided real *)
fun real_to_string_for_json num =
  if num < 0.0 
  then "-" ^ real_to_string(real_abs(num)) 
  else real_to_string num

(* 19 String representation of provided json *)
fun json_to_string json =
  let 
    fun wrap_bracket str =
      "[" ^ str ^ "]"
    
    (* isolate the recursion to make adding braces easy *) 
    fun getStr json =
      let 
        fun process_object json = 
          case json of
            Object(obj) => ( case obj of
                               [] => ""
                               | (field,value)::[] =>  quote_string(field) ^ " : " ^ getStr(value) 
                               | (field,value)::json' =>  quote_string(field) ^ " : " ^ getStr(value) ^ 
                                                               ", " ^ (process_object(Object json'))  )
            | _ => ""
    
        fun process_list json = 
          case json of
            Array(arr) => ( case arr of
                              [] => ""
                              | front::[] => getStr(front)
                              | front::json' => getStr(front) ^ ", " ^ process_list(Array json') )
            | _ => ""
      in        
        case json of
          Num n => real_to_string_for_json n
            | String (st) => quote_string st
            | True => "true"
            | False => "false"
            | Null => "null"
            | Array(a) => let val arrStr = process_list json in wrap_bracket(arrStr) end
            | Object(a) => process_object json
      end
  in
    "{" ^ getStr json ^ "}"
  end
  

(* For CHALLENGE PROBLEMS, see hw2challenge.sml *)

