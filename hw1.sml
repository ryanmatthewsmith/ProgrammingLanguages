(* ryan matthew smith . rsmith24 . HW1 341 SP17 . functions *)

(* dates should be formatted DAY * MONTH * YEAR *)

(* 1 function evaluates to true if the first date comes before the second *)
fun is_older ( date1 : int*int*int , date2 : int*int*int) =
  (*year*)
  if #3 date1 < #3 date2
  then true
  else 
    (*month*)
    if #3 date1 = #3 date2 andalso #2 date1 < #2 date2
    then true
    else
      (*day*)
      if #3 date1 = #3 date2 andalso #2 date1 = #2 date2 andalso #1 date1 < #1 date2
      then true
      else false

(* 2 function evaluates number of occurances of a month given a list of dates *)
fun number_in_month (dates : (int*int*int) list, month : int) =
  (*empty list base case, returns 0*)
  if null dates
  then 0
  else 
    if #2(hd dates) = month
    then 1 + number_in_month(tl dates, month)
    else number_in_month(tl dates, month)

(* 3 function evaluates number of occurances of a list of months given a list of dates *)
fun number_in_months (dates : (int*int*int) list, months : int list) =
  if null months
  then 0
  else
    number_in_month(dates, hd months) + number_in_months(dates, tl months)
   
(* 4 function returns a list of dates falling in month for a given month and list of dates *)
fun dates_in_month (dates : (int*int*int) list, month : int) =
  if null dates
  then []
  else
    let val datesHead = hd dates
    in
      if #2 datesHead = month
      then datesHead :: dates_in_month(tl dates,month)
      else dates_in_month(tl dates,month)
    end

(* 5 funtion returns a list of dates falling in list of months, given both args *)
fun dates_in_months (dates : (int*int*int) list, months : int list) =
  if null months
  then []
  else dates_in_month(dates, hd months) @ dates_in_months(dates, tl months)

(* 6 returns the nth string in the list *)
fun get_nth (str : string list, n : int) = 
  if n < 1
  then "n must be positive"
  else
    if null (str)
    then "n is larger than list bounds"
    else
      if n = 1
      then hd str
      else get_nth (tl str, n-1)

(* 7 returns string example of date arg *)
fun date_to_string (date : int*int*int) =
  let val monthList = ["January","February","March","April","May",
                 "June","July","August","September",
                 "October","November","December"]
  in
    "" ^ get_nth(monthList, #2 date) ^ "-" ^ Int.toString(#1 date) ^ "-" ^ Int.toString(#3 date)
  end

(* 8 returns index of last digit that sums list to less than sum arg, given list arg *)
fun number_before_reaching_sum (sum : int, posList : int list) =
  if (sum - hd posList) < 1
  then 0
  else 1 + number_before_reaching_sum(sum - hd posList, tl posList)

(* 9 returns month the number of days appears in a year as an int  *)
fun what_month (days : int) =
  let val daysPerMonth = [31,28,31,30,31,30,31,31,30,31,30,31]
  in
    1 + number_before_reaching_sum(days, daysPerMonth)
  end

(* 10 returns list of month days fall in between day1 and day2 *)
fun month_range (day1 : int, day2 : int) =
  if day1 > day2
  then []
  else what_month(day1)::month_range(day1+1, day2)

(* 11 returns oldest date from a list of dates *)
fun oldest (dates : (int * int * int) list) =
  if null dates
  then NONE
  else
    let fun oldestInt (dates : (int * int * int) list) =
      if null (tl dates)
      then hd dates
      else
        if is_older(hd dates, hd (tl dates))
        then if null (tl(tl dates))
             then hd dates 
             else oldestInt(hd dates::(tl(tl dates))) 
        else oldestInt(tl dates)
    in
      SOME (oldestInt(dates))
    end

(* 12 returns a list of partial sums given a list of ints *)
fun cumulative_sum (intList : int list) =
  if null intList 
  then []
  else
    if null (tl intList) 
    then intList
    else
      hd intList :: cumulative_sum (hd intList + hd (tl intList) :: tl (tl intList))

(* remove list dupes *)
fun remove_int_dupes (intList : int list) =
  (* this solution seems slow / changes order but works *)
  (* first function looks for any match in list *)
  let fun look_for_dupe(head : int, intList : int list) =
    if null intList
    then false
    else
      if hd intList = head
      then true
      else look_for_dupe (head, tl intList)

    (* next function cuts head off if a dupe was found *)
    fun remove_dupe_head (intList : int list) =
      if null intList
      then []
      else
        if look_for_dupe( hd intList, tl intList)
        then remove_dupe_head(tl intList)
        else hd intList::remove_dupe_head(tl intList)
    in
      remove_dupe_head(intList)
    end

(* 13a number_in_months that handles duplicates *)
fun number_in_months_challenge (dates : (int*int*int) list, months : int list) =
  let val trimmedMonths = remove_int_dupes(months)
  in
    number_in_months(dates,trimmedMonths)
  end

(* 13b dates_in_months that handles duplicates *)
fun dates_in_months_challenge (dates : (int*int*int) list, months : int list) =
  let val trimmedMonths = remove_int_dupes(months)
  in
    dates_in_months(dates, trimmedMonths)
  end

(* 14 checks for a real date, leap year included *)
fun reasonable_date(date : (int*int*int)) =
  let val daysPerMonth = (31,28,31,30,31,30,31,31,30,31,30,31)
  val year = #3 date
  val month = #2 date
  val day = #1 date
  in
    (* year valid *)
    if year < 1
    then false
    else
      (* month valid *)
      if month < 1 orelse month > 12
      then false
      else
        (* day valid *)
        if day < 1 orelse day > 31
        then false
        else
          (*non feb 30 days *)
          if day = 31 andalso month = 4 orelse month = 6 orelse month = 9 orelse month = 11
          then false
          else
            (*feb case*)
            if day > 28 andalso month = 2
            then 
              if day = 29 andalso ((year mod 4 = 0 andalso year mod 100 <> 0)  orelse year mod 400 = 0)
              then true
              else false
            else true 
  end
