# University of Washington, Programming Languages, Homework 6, hw6runner.rb

# This is the only file you turn in, so do not modify the other files as
# part of your solution.
# Ryan Smith, rsmith24

class MyPiece < Piece
  
  # The constant All_My_Pieces should be declared here
  All_My_Pieces= All_Pieces + [rotations([[0, 0], [-1, 0], [0, 1], [-1, 1], [1, 1]]), #square + lower right
                                        [[[0, 0], [-1, 0], [1, 0], [-2, 0], [2, 0]],   #long + 1
                                         [[0, 0], [0, -1], [0, 1], [0, -2], [0, 2]]],
                               rotations([[0, 0], [0, 1], [1, 1]])]                    #small angle
  
  #override next piece
  def self.next_piece (board)
      MyPiece.new(All_My_Pieces.sample, board)
  end

  def self.cheat_piece (board)
      MyPiece.new([[0,0]], board)
  end    

end

class MyBoard < Board
  
  #override initialize
  def initialize (game)
    super
    @current_block = MyPiece.next_piece(self)
    @cheat_min = 100
    @cheat_curr = false
  end

  #override next_piece
  def next_piece
    if @cheat_curr
      @cheat_curr = false
      @current_block = MyPiece.cheat_piece(self)
      @current_pos = nil
    else   
      @current_block = MyPiece.next_piece(self)
      @current_pos = nil     
    end
  end

  def store_current
    locations = @current_block.current_rotation
    piece_size = locations.size
    displacement = @current_block.position
    (0..(piece_size - 1)).each{|index|
      current = locations[index];
      @grid[current[1]+displacement[1]][current[0]+displacement[0]] =
      @current_pos[index]
    }
    remove_filled
    @delay = [@delay -2, 80].max
  end

  #rotates the current piece 180 degrees
  def rotate_180
    if !game_over? and @game.is_running?
      @current_block.move(0, 0, -2)
    end
    draw
  end

  #allows cheat piece
  def cheat
    if (@score >= @cheat_min) & !@cheat_curr
      @cheat_curr = true
      @score -= @cheat_min
    end
  end

end

class MyTetris < Tetris  
  
  #set board with MyBoard
  def set_board
    @canvas = TetrisCanvas.new
    @board = MyBoard.new(self)
    @canvas.place(@board.block_size * @board.num_rows + 3,
                  @board.block_size * @board.num_columns + 6, 24, 80)
    @board.draw
  end

  #u bound to 180 degree rotation, c cheat
  def key_bindings
    super
    @root.bind('u', proc {@board.rotate_180})
    @root.bind('c', proc {@board.cheat})
  end

end

class MyPieceChallenge < MyPiece
end

class MyBoardChallenge < MyBoard

  def initialize (game)
    super
    @top_speed = 50
    @min_speed = 750
  end

  def slow_down
    if @delay >= @min_speed
      return
    else 
      @delay += 50
    end
  end

  def speed_up
    if @delay <= @top_speed
      return
    else
      @delay -= 50
    end
  end
  
end

class MyTetrisChallenge < MyTetris
  
  #set board with MyBoardChallenge
  def set_board
    @canvas = TetrisCanvas.new
    @board = MyBoardChallenge.new(self)
    @canvas.place(@board.block_size * @board.num_rows + 3,
                  @board.block_size * @board.num_columns + 6, 24, 80)
    @board.draw
  end

  #add speedup slowdown buttons
  def key_bindings
    super
    @root.bind('g', proc {@board.slow_down})
    @root.bind('f', proc {@board.speed_up})
  end

  #speed buttons
  def buttons
    super
    
    slow = TetrisButton.new('-', 'lightgreen'){@board.slow_down}
    slow.place(35, 50, 27, 501)

    speed = TetrisButton.new('+', 'lightgreen'){@board.speed_up}
    speed.place(35, 50, 127, 501)
  end

end
